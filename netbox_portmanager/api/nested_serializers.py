from rest_framework import serializers

from netbox.api.serializers import WritableNestedSerializer

from ..models import DeviceGroup, ChangeLog, Secret


class NestedDeviceGroupSerializer(WritableNestedSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='plugins:netbox_portmanager:devicegroup')

    class Meta:
        model = DeviceGroup
        fields = '__all__'


class NestedChangeLogSerializer(WritableNestedSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='plugins:netbox_portmanager:changelog')

    class Meta:
        model = ChangeLog
        fields = '__all__'


class NestedSecretSerializer(WritableNestedSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='plugins:netbox_portmanager:secret')

    class Meta:
        model = Secret
        fields = '__all__'
