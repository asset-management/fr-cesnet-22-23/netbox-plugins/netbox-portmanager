import django_tables2 as tables

from dcim.models import Device
from netbox.tables import NetBoxTable
from netbox.tables.columns import TagColumn, ActionsColumn
from dcim.tables import DeviceTable as NetboxDeviceTable
from django.contrib.auth.models import User, Group


from .template_code import *
from .models import DeviceGroup, ChangeLog, Secret
from .utils import generate_attrs
from .constants import CHANGELOG_DATETIME_FORMAT


class InterfaceTable(tables.Table):
    if_name = tables.Column(
        order_by=("name", ),
        verbose_name="Name",
        attrs=generate_attrs("col-name", "Interface name."),
    )
    if_description = tables.TemplateColumn(
        IF_TABLE_DESC,
        verbose_name="Description",
        attrs=generate_attrs("col-description", "Interface description."),
    )
    if_state = tables.TemplateColumn(
        IF_TABLE_STATE,
        verbose_name="State",
        attrs=generate_attrs("col-state", "Interface state."),
    )
    if_admin_status = tables.TemplateColumn(
        IF_TABLE_ADMIN_STATUS,
        verbose_name="Admin Status",
        attrs=generate_attrs("col-admin-status", "Interface admin status."),
    )
    if_ps_max = tables.TemplateColumn(
        IF_TABLE_PS_MAX,
        verbose_name="MAX Port Security",
        attrs=generate_attrs("col-max-ps", "Maximal interface port-security."),
    )
    if_poe_consum = tables.Column(
        verbose_name="PoE Consum (W)",
        attrs=generate_attrs("col-poe", "Actual power consumption."),
    )
    if_poe_alloc = tables.Column(
        verbose_name="PoE Alloc (W)",
        attrs=generate_attrs("col-poe", "Alocated interface power."),
    )
    if_vlan = tables.TemplateColumn(
        IF_TABLE_VLAN,
        verbose_name="VLAN",
        attrs=generate_attrs("col-vlan", "Interface access VLAN."),
    )
    if_index = tables.TemplateColumn(
        IF_TABLE_SNMP_INDEX,
        verbose_name="",
        attrs=generate_attrs("col-index", "Interface SNMP-index."),
    )
    if_macs = tables.TemplateColumn(
        IF_TABLE_SHOW_MACS,
        verbose_name="Show MACs",
        attrs=generate_attrs("col-show-mac", "Show MAC addresses on the interface."),
    )

    class Meta:
        attrs = {
            "class": "table table-hover object-list",
        }
        row_attrs = {
            "style": lambda record: ""
            if record.get("if_enabled")
            else "opacity: 0.65;",
            "title": lambda record: ""
            if record.get("if_enabled")
            else record.get("if_comment"),
        }

        fields = (
            "if_name",
            "if_vlan",
            "if_description",
            "if_ps_max",
            "if_admin_status",
            "if_state",
            "if_poe_consum",
            "if_poe_alloc",
            "if_macs",
            "if_index",
        )
        orderable = False
        empty_text = (
            "Cannot receive response from the device. Please contact the administrator."
        )


class DeviceGroupTable(NetBoxTable):
    name = tables.TemplateColumn(
        order_by=("name",), 
        template_code=DGT_DEVICE_GROUP, 
        verbose_name="Name",
    )
    description = tables.Column(verbose_name='description')
    vlans = tables.Column(verbose_name="VLANs")
    portsec_max = tables.Column(verbose_name="Portsec MAX")
    devices = tables.TemplateColumn(
        template_code=DGT_DEVICES,
        verbose_name="Devices",
        
    )
    user_groups = tables.TemplateColumn(
        template_code=DGT_USER_GROUPS,
        verbose_name="User Groups",
        
    )
    users = tables.TemplateColumn(
        template_code=DGT_USERS,
        verbose_name="Users",
        
    )
    tags = TagColumn(
        url_name='plugins:netbox_portmanager:devicegroup_list'
    )

    class Meta(NetBoxTable.Meta):
        model = DeviceGroup
        fields = ('pk', 'id', 'name', 'description', 'vlans', 'portsec_max', 'devices', 'user_groups', 'users', 'tags', )
        default_columns = ('pk', 'name', 'description', 'vlans', 'portsec_max', )


class ChangeLogTable(NetBoxTable):
    datetime = tables.DateTimeColumn(
        verbose_name="Date Time",
        format=CHANGELOG_DATETIME_FORMAT,
    )
    status = tables.TemplateColumn(verbose_name="Status", template_code=CLT_STATUS)
    user = tables.Column(verbose_name="User")
    device = tables.Column(
        verbose_name='Device',
    )
    interface = tables.Column(verbose_name='Interface')
    component = tables.Column(verbose_name='Component')
    before = tables.Column(verbose_name='Before')
    after = tables.Column(verbose_name='After')
    details = tables.Column(verbose_name='Details')
    actions = ActionsColumn(actions=())

    class Meta(NetBoxTable.Meta):
        model = ChangeLog
        fields = (
            'pk',
            'id',
            'datetime',
            'status',
            'user',
            'device',
            'interface',
            'component',
            'before',
            'after',
            'details',
        )
        default_columns = (
            'pk',
            'id',
            'datetime',
            'status',
            'user',
            'device',
            'interface',
            'component',
            'before',
            'after',
            'details',
        )


class DeviceTable(NetboxDeviceTable):
    name = tables.TemplateColumn(
        order_by=('_name',), 
        template_code=DT_DEVICE,
    )
    device_groups = tables.TemplateColumn(
        verbose_name='Device Groups', template_code=DT_DEVICE_GROUPS
    )
    actions = ActionsColumn(actions=())

    class Meta(NetboxDeviceTable.Meta):
        model = Device
        default_columns = (
            'pk',
            'name',
            'site',
            'location',
            'rack',
            'manufacturer',
            'device_type',
        )


class SecretTable(NetBoxTable):
    name = tables.TemplateColumn(
        verbose_name='Name',
        template_code=CST_NAME,
    )
    description = tables.Column(verbose_name='Description')
    tags = TagColumn(
        url_name='plugins:netbox_portmanager:secret_list'
    )
    devices = tables.TemplateColumn(
        verbose_name='Devices',
        template_code=CST_DEVICES,
    )

    class Meta(NetBoxTable.Meta):
        model = Secret
        fields = ('pk', 'name', 'description', 'devices', 'tags', )
        default_columns = ('pk', 'name', 'description', )


class UserTable(NetBoxTable):
    id = tables.Column(
        verbose_name='ID',
    )
    username = tables.TemplateColumn(verbose_name='Username', template_code=UT_USERNAME)
    first_name = tables.Column(verbose_name='First Name')
    last_name = tables.Column(verbose_name='Last Name')
    email = tables.Column(verbose_name='Email')
    actions = ActionsColumn(actions=())

    class Meta(NetBoxTable.Meta):
        model = User
        fields = (
            'pk',
            'username',
            'first_name',
            'last_name',
            'email',
        )
        default_columns = fields


class UserGroupTable(NetBoxTable):
    id = tables.Column(
        verbose_name='ID',
    )
    name = tables.TemplateColumn(verbose_name='Name', template_code=UGT_NAME)
    actions = ActionsColumn(actions=())
    users = tables.TemplateColumn(verbose_name='Users', template_code=UGT_USERS)

    class Meta(NetBoxTable.Meta):
        model = Group
        fields = (
            'pk',
            'name',
            'users',
        )
        default_columns = (
            'pk',
            'name',
        )
