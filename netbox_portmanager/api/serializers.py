from netbox.api.serializers import NetBoxModelSerializer
from users.api.nested_serializers import NestedUserSerializer, NestedGroupSerializer

from ..models import DeviceGroup, ChangeLog, Secret
from ..api.nested_serializers import *


class DeviceGroupSerializer(NetBoxModelSerializer):
    users = NestedUserSerializer(
        many=True,
    )
    user_groups = NestedGroupSerializer(
        many=True,
    )
    
    class Meta:
        model = DeviceGroup
        fields = '__all__'


class ChangeLogSerializer(NetBoxModelSerializer):

    class Meta:
        model = ChangeLog
        fields = '__all__'


class SecretSerializer(NetBoxModelSerializer):

    class Meta:
        model = Secret
        fields = '__all__'


