from netbox.api.viewsets import NetBoxModelViewSet
from ..models import DeviceGroup, ChangeLog, Secret
from .serializers import DeviceGroupSerializer, ChangeLogSerializer, SecretSerializer


class DeviceGroupViewSet(NetBoxModelViewSet):
    queryset = DeviceGroup.objects.all()
    serializer_class = DeviceGroupSerializer


class ChangeLogViewSet(NetBoxModelViewSet):
    queryset = ChangeLog.objects.all()
    serializer_class = ChangeLogSerializer


class SecretViewSet(NetBoxModelViewSet):
    queryset = Secret.objects.all()
    serializer_class = SecretSerializer
