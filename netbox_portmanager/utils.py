from django.contrib.auth.models import User
from django.db.models import Q

from typing import Optional, Tuple, Dict, Union
from datetime import datetime

from dcim.models import Device
from utilities.permissions import get_permission_for_model

from .models import DeviceGroup, Secret
from .portmanager.switch_manager import SwitchManager, GeneralSwitch
from .constants import DEVICE_CF_NAME, DEVICE_FQDN_ATTR, GLOBAL_COMMUNITY_STRING, LOGGER, SESSION_TIMEOUT


def generate_attrs(tag_class: str, th_title: str):
    return {
        "td": {
            "class": tag_class,
        },
        "th": {
            "title": th_title,
        },
    }


def get_switch(
    fqdn: str,
    community_string: str,
    switch_type: str,
    portsec_max: int = 16384,
    vlan_intervals: str = "[]",
) -> GeneralSwitch:
    switch_class = SwitchManager.get_switch_class(type_name=switch_type)
    return switch_class(
        fqdn=fqdn,
        community_string=community_string,
        vlan_intervals=vlan_intervals,
        portsec_max=portsec_max,
    )


def get_device_and_devicegroups(
    user: User, queryset, device_pk: int
) -> Optional[Tuple[Device, DeviceGroup]]:
    try:
        device = queryset.get(pk=device_pk)
        device_groups = DeviceGroup.objects.filter(devices=device)
        if not user.is_superuser:
            device_groups = device_groups.filter(users=user)
    except:
        return None
    if not device_groups:
        return None
    return device, device_groups


def get_user_label(user: User) -> str:
    label = user.username
    names = " ".join([name for name in (user.first_name, user.last_name) if len(name) > 0])
    if len(names) > 0:
        label += f" ({names})"
    return label


def get_device_fqdn(device: Device) -> Optional[str]:
    if vc := device.virtual_chassis:
        return vc.name
    fqdn_attr = DEVICE_FQDN_ATTR
    fqdn = getattr(device, fqdn_attr, None)
    if fqdn is None:
        LOGGER.error(f"Attribute ({fqdn_attr}) does not exist.")
        return None
    return str(fqdn)


def get_portmanager_tab_label(device: Device) -> str:
    if DeviceGroup.objects.filter(devices=device).exists():
        return "🔑"
    return None


def get_community_string(device: Device) -> Optional[str]:
    if (community_string_pk := device.custom_field_data.get(DEVICE_CF_NAME)) is None:
        return get_default_community_string()
    if (secret := Secret.objects.get(pk=community_string_pk)._secret) == "":
        return get_default_community_string()
    return secret


def get_default_community_string() -> Optional[str]:
    if GLOBAL_COMMUNITY_STRING is None:
        LOGGER.error("Cannot find default community string.")
        return None
    return GLOBAL_COMMUNITY_STRING


def get_device_data(device: Device, user: User) -> Optional[Dict[str, Union[str, int]]]:
    device_groups = DeviceGroup.objects.filter(
        Q(user_groups__in=user.groups.all()) |
        Q(users=user)
    ).distinct()
    
    if not device_groups.exists():
        LOGGER.error(f"User ({user.username}) is not assigned to any User Group.")
        return None

    devices = Device.objects.filter(pk=device.pk, device_groups__in=device_groups.all()).distinct()
    if not devices.exists():
        LOGGER.error(f"Device ({device.name}) is not assigned to any users Device Group.")
        return None

    vlans, portsec_max = ['[1-4094]'], 16384
    if not user.has_perm(get_permission_for_model(Device, 'portmanager-admin')):
        vlans = [device_group.vlans for device_group in device_groups]
        portsec_max = max(*[device_group.portsec_max for device_group in device_groups], 1)
    if (community_string := get_community_string(device)) is None:
        LOGGER.error(f"Cannot find community string for device ({device}).")
        return None
    if (fqdn := get_device_fqdn(device)) is None:
        LOGGER.error(f"Cannot find fqdn attribute for device ({device})")
        return None
    
    return {
        "vlans": vlans,
        "portsec_max": portsec_max,
        "community_string": community_string,
        "fqdn": fqdn
    }


def get_session_timestamp():
    return datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def check_session_timestamp(timestamp):
    actual_time = datetime.now()
    loaded_time = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
    time_diff = actual_time - loaded_time

    return time_diff.total_seconds() < SESSION_TIMEOUT
