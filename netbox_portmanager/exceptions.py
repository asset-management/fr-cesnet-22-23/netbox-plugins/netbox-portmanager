class CommunityStringNotFound(Exception):
    def __init__(self, device_group: str) -> None:
        message = f"Cannot obtain Community String for DeviceGroup: {device_group}"
        super().__init__(message)