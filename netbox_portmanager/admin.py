from django.contrib import admin

from .models import DeviceGroup, ChangeLog
from extras.models import ObjectChange

@admin.register(DeviceGroup)
class PMDeviceGroupAdmin(admin.ModelAdmin):
    list_display = ("name", )


@admin.register(ChangeLog)
class PMChangeLogAdmin(admin.ModelAdmin):
    list_display = ("id", )

@admin.register(ObjectChange)
class PMssAdmin(admin.ModelAdmin):
    list_display = ("id", )
