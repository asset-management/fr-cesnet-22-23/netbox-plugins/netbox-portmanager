from django.urls import path
from . import views, models
from netbox.views.generic import ObjectChangeLogView, ObjectJournalView


urlpatterns = [
    ## Device
    path("device/", views.DeviceListView.as_view(), name="device_list"),
    ## Interface Mac
    path("dcim/device/<int:device_pk>/interface/<int:if_index>/vlan/<int:vlan_id>/", views.InterfaceVlanMacView.as_view(), name="interface_mac"),
    ## Change Log
    path("change-log/", views.ChangeLogListView.as_view(), name="changelog_list"),
    path("change-log/<int:pk>/", views.ChangeLogView.as_view(), name="changelog"),
    ## User Change Log
    path('user-change-log/', views.UserChangeLogListView.as_view(), name='userchangelog_list'),
    path('change-log/<int:pk>/', views.UserChangeLogView.as_view(), name='userchangelog'),
    ## Device Group
    path('device-group/', views.DeviceGroupListView.as_view(), name='devicegroup_list'),
    path('device-group/add/', views.DeviceGroupEditView.as_view(), name='devicegroup_add'),
    path('device-group/<int:pk>/', views.DeviceGroupView.as_view(), name='devicegroup'),
    path('device-group/<int:pk>/edit/', views.DeviceGroupEditView.as_view(), name='devicegroup_edit'),
    path('device-group/<int:pk>/delete/', views.DeviceGroupDeleteView.as_view(), name='devicegroup_delete'),
    path('device-group/<int:pk>/journal/', ObjectJournalView.as_view(), name='devicegroup_journal', kwargs={'model': models.DeviceGroup}),
    path('device-group/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='devicegroup_changelog', kwargs={'model': models.DeviceGroup}),
    ## User
    path('user/', views.UserListView.as_view(), name="user_list"),
    path('user/<int:pk>/', views.UserView.as_view(), name="user"),
    ## User Group
    path('user-group/', views.UserGroupListView.as_view(), name="usergroup_list"),
    path('user-group/<int:pk>/', views.UserGroupView.as_view(), name="usergroup"),
    ## User Info
    path('user-info/', views.UserInfoView.as_view(), name='userinfo'),
    ## Community String
    path("secret/", views.SecretListView.as_view(), name="secret_list"),
    path("secret/add/", views.SecretEditView.as_view(), name="secret_add"),
    path("secret/<int:pk>/", views.SecretView.as_view(), name="secret"),
    path("secret/<int:pk>/edit", views.SecretEditView.as_view(), name="secret_edit"),
    path("secret/<int:pk>/delete", views.SecretDeleteView.as_view(), name="secret_delete"),
    path('secret/<int:pk>/journal/', ObjectJournalView.as_view(), name='secret_journal', kwargs={'model': models.Secret}),
    path('secret/<int:pk>/changelog/', ObjectChangeLogView.as_view(), name='secret_changelog', kwargs={'model': models.Secret}),
]
