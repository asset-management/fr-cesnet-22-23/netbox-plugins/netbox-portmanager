from typing import List


class SwitchTypeNotFound(Exception):
    def __init__(self, type_name: str) -> None:
        message = f"Switch type ({type_name}) not found."
        super().__init__(message)


class VLANIntervalSyntaxError(Exception):
    def __init__(self, interval_string: str) -> None:
        message = f"VLANInterval bad format ({interval_string})."
        super().__init__(message)


class SNMPError(Exception):
    def __init__(self, snmp_type: str, oids: List[str],message: str) -> None:
        super().__init__(f"{snmp_type} ({oids}): {message}")


class DataIntegrityError(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
