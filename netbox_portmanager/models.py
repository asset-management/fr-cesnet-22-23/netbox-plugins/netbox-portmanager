from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User, Group
from django.conf import settings

from netbox.models import RestrictedQuerySet, CloningMixin, ChangeLoggingMixin, NetBoxModel
from dcim.models import Device

from .exceptions import CommunityStringNotFound
from .constants import DEVICE_CF_NAME

PLUGIN_CONFIG = settings.PLUGINS_CONFIG.get("netbox_portmanager", {})


class Secret(NetBoxModel):
    name = models.CharField(max_length=40, unique=True)
    description = models.CharField(max_length=100)
    _secret = models.CharField(
        max_length=100,
        default="",
    )

    def __str__(self) -> str:
        return self.name

    @property
    def secret(self):
        return self._secret

    def get_absolute_url(self):
        return reverse("plugins:netbox_portmanager:secret", args=[self.pk])

    def get_assigned_devices(self):
        kwargs = {
            f"custom_field_data__{DEVICE_CF_NAME}": self.pk,
        }
        return Device.objects.filter(**kwargs)


class DeviceGroup(NetBoxModel):
    name = models.CharField(max_length=40, unique=True)
    description = models.CharField(max_length=100)
    devices = models.ManyToManyField(to=Device, related_name="device_groups")
    vlans = models.CharField(
        default="[]",
        max_length=300
    )
    users = models.ManyToManyField(to=User, related_name="device_groups")
    user_groups = models.ManyToManyField(to=Group, related_name="device_groups")
    portsec_max = models.IntegerField(
        default=1
    )

    def __str__(self) -> str:
        return self.name

    def get_absolute_url(self):
        return reverse("plugins:netbox_portmanager:devicegroup", args=[self.pk])

  
class ChangeLog(CloningMixin, models.Model):
    status = models.CharField(max_length=255, editable=False, default="UNKNOWN")
    user_object = models.ForeignKey(
        to=User, 
        verbose_name='change_logs',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )
    user = models.CharField(
        max_length=255,
        editable=False,
    )
    datetime = models.DateTimeField(auto_now_add=True, editable=False, db_index=True)
    device = models.CharField(
        max_length=255,
        editable=False,
    )
    interface = models.CharField(
        max_length=255,
        editable=False,
        blank=True,
        null=True,
    )
    component = models.CharField(
        max_length=255,
        editable=False,
        blank=True,
        null=True,
    )
    before = models.CharField(
        max_length=255,
        editable=False,
        blank=True,
        null=True,
    )
    after = models.CharField(
        max_length=255,
        editable=False,
        blank=True,
        null=True,
    )
    details = models.CharField(
        max_length=1024,
        editable=False,
        blank=True,
        null=True,
    )

    objects = RestrictedQuerySet.as_manager()

    class Meta:
        ordering = ["-datetime"]
        verbose_name = "Change Log"
        verbose_name_plural = "Change Logs"

    def get_absolute_url(self):
        return reverse("plugins:netbox_portmanager:changelog", args=[self.pk])
