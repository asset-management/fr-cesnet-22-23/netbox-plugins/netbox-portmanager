from extras.plugins import PluginConfig


class NetBoxPortmanager(PluginConfig):
    name = "netbox_portmanager"
    verbose_name = "Portmanager"
    description = "Plugin for ports managing"
    version = "0.3.5"
    author = "Michal Drobný"
    author_email = "drobny@ics.muni.cz"
    base_url = "netbox_portmanager"
    min_version = "3.5.0"
    required_settings = []
    default_settings = {
        "global_community_string": "private",
        "changelog_datetime_format": "Y-d-m H:i:s",
        "device_fqdn_attr": "name",
        "device_cf_name": "portmanager_community_string",
        "default_dg_prefix": "",
        "default_dg_sufix": "",
        "session_timeout": 60 * 60 * 12,
        "snmp_timeout": 30,
    }


config = NetBoxPortmanager
