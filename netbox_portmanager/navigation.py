from extras.plugins import PluginMenuButton, PluginMenuItem, PluginMenu
from utilities.choices import ButtonColorChoices


DEVICES_ITEM = PluginMenuItem(
    link="plugins:netbox_portmanager:device_list",
    link_text="Devices",
    permissions=["dcim.portmanager_device"],
)


USERINFO_ITEM = PluginMenuItem(
    link="plugins:netbox_portmanager:userinfo",
    link_text="User Info",
    permissions=["dcim.portmanager_device"],
)


USERCHANGELOGS_ITEM = PluginMenuItem(
    link='plugins:netbox_portmanager:userchangelog_list',
    link_text='User Logs',
    permissions=['dcim.portmanager_device'],
)


DEVICEGROUPS_ITEM = PluginMenuItem(
    link="plugins:netbox_portmanager:devicegroup_list",
    link_text="Device Groups",
    permissions=["netbox_portmanager.view_devicegroup"],
    buttons=(
        PluginMenuButton(
            link="plugins:netbox_portmanager:devicegroup_add",
            title="Add",
            icon_class="mdi mdi-plus-thick",
            color=ButtonColorChoices.GREEN,
            permissions=["netbox_portmanager.add_devicegroup"],
        ),
    ),
)


USERGROUPS_ITEM = PluginMenuItem(
    link="plugins:netbox_portmanager:usergroup_list",
    link_text="User Groups",
    permissions=["netbox_portmanager.view_devicegroup"],
)


USERS_ITEM = PluginMenuItem(
    link="plugins:netbox_portmanager:user_list",
    link_text="Users",
    permissions=["netbox_portmanager.view_devicegroup"],
)


COMMUNITYSTRINGS_ITEM = PluginMenuItem(
    link="plugins:netbox_portmanager:secret_list",
    link_text="Secrets",
    permissions=["netbox_portmanager.view_secret"],
    buttons=(
        PluginMenuButton(
            link="plugins:netbox_portmanager:secret_add",
            title="Add",
            icon_class="mdi mdi-plus-thick",
            color=ButtonColorChoices.GREEN,
            permissions=["netbox_portmanager.add_secret"],
        ),
    ),
)


CHANGELOGS_ITEM = PluginMenuItem(
    link="plugins:netbox_portmanager:changelog_list",
    link_text="Change Logs",
    permissions=["netbox_portmanager.view_changelog"],
)


menu = PluginMenu(
    label="Portmanager",
    groups=(
        (
            "User Section",
            (
                DEVICES_ITEM,
                USERINFO_ITEM,
                USERCHANGELOGS_ITEM,
            ),
        ),
        (
            "Admin Section",
            (
                DEVICEGROUPS_ITEM,
                USERGROUPS_ITEM,
                USERS_ITEM,
                COMMUNITYSTRINGS_ITEM,
                CHANGELOGS_ITEM,
            ),
        ),
    ),
    icon_class="mdi mdi-web",
)
