from .general_switch import GeneralSwitch
from .cisco_switch import CiscoSwitch
from .exceptions import SwitchTypeNotFound

class SwitchManager:
    __SWITCH_DICT = {
        "cisco": CiscoSwitch
    }

    @staticmethod
    def get_switch_class(type_name: str) -> GeneralSwitch:
        switch_class = SwitchManager.__SWITCH_DICT.get(type_name, None)
        if switch_class is None:
            raise SwitchTypeNotFound(switch_class)
        return switch_class
