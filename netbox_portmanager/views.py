from django.views.generic import View
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth.models import User, Group
from django.conf import settings
from django.utils.translation import gettext as _
from django.db.models import Q

from datetime import datetime

from dcim.filtersets import DeviceFilterSet
from dcim.models import Device
from netbox.views import generic

from utilities.views import register_model_view, ViewTab
from utilities.permissions import get_permission_for_model

from .models import DeviceGroup, ChangeLog, Secret
from .filtersets import DeviceGroupFilterSet, ChangeLogFilterSet, SecretFilterSet
from .forms.forms import (
    DeviceGroupForm,
    SecretForm,
)
from .tables import (
    DeviceGroupTable,
    DeviceTable,
    InterfaceTable,
    ChangeLogTable,
    SecretTable,
    UserTable,
    UserGroupTable
)
from .portmanager.cisco_switch import CiscoSwitch
from .portmanager.switch_manager import GeneralSwitch
from .portmanager.exceptions import SNMPError, DataIntegrityError
from .utils import get_switch, check_session_timestamp, get_user_label, get_session_timestamp, get_portmanager_tab_label, get_device_data
from .constants import LOGGER, DEVICE_CF_NAME


from typing import Union, Any, Dict, List, Tuple

PLUGIN_CONFIG = settings.PLUGINS_CONFIG.get("netbox_portmanager", {})
POST_DATA = Dict[str, List[Union[str, int]]]


#
# Device
#

class DeviceListView(generic.ObjectListView):
    queryset = Device.objects.exclude(
        device_groups__isnull=True
    )
    queryset_dg = DeviceGroup.objects.all()
    template_name = "generic/object_list.html"
    table = DeviceTable
    actions = ()
    filterset = DeviceFilterSet

    def get_required_permission(self):
        return get_permission_for_model(Device, "portmanager")

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        if user.has_perms((perm,)):
            self.queryset = self.queryset.filter(
                Q(device_groups__users=user) | 
                Q(device_groups__user_groups__in=user.groups.all())
            ).distinct()
            return True
        return False


@register_model_view(Device, "portmanager", "portmanager")
class DeviceView(generic.ObjectView):
    queryset = Device.objects.all()
    queryset_dg = DeviceGroup.objects.all()
    template_name = "netbox_portmanager/portmanager.jinja"
    actions = ()

    tab = ViewTab(
        label=_("Portmanager"),
        badge=get_portmanager_tab_label,
        permission="dcim.portmanager_device",
        weight=10000,  # Expresses tab order. The larger value, the more to the right.
        hide_if_empty=True,
    )

    def get_required_permission(self):
        return get_permission_for_model(Device, "portmanager")

    def try_load_main_data(self, switch) -> bool:
        try:
            switch.load_main_data()
            return True
        except SNMPError as e:
            LOGGER.error(f"SNMPError: {e}")
            return redirect("home")
        except Exception as e:
            LOGGER.error(f"Exception Error: {e}")
        return False


    def get(self, request, pk):
        device = self.get_object(pk=pk)
        if (device_data := get_device_data(device, request.user)) is None:
            self.handle_no_permission()

        switch: CiscoSwitch = get_switch(
            fqdn=device_data["fqdn"],
            community_string=device_data["community_string"],
            switch_type=device.device_type.manufacturer.name.lower(),
            portsec_max=device_data["portsec_max"],
            vlan_intervals=device_data["vlans"],
        )

        if not self.try_load_main_data(switch):
            messages.warning(request, f"Cannot load data from device.")
            messages.warning(
                request, "[FAILURE] Please try again or contact administrator."
            )
            return redirect("home")

        
        table = self.create_and_setup_table(switch, device)
        return render(
            request,
            self.get_template_name(),
            {
                "object": device,
                "table": table,
                "show_poe": switch.show_poe,
                "available_poe": switch.available_poe,
                "used_poe": switch.used_poe,
                "remaining_poe": switch.remaining_poe,
                "tab": self.tab,
                "load_timestamp": get_session_timestamp()
            },
        )

    def post(self, request, pk):
        if not "_deploy" in request.POST and not "_deploy_save" in request.POST:
            messages.warning(
                request, "[ERROR] Please try again or contact administrator."
            )
            return redirect("home")
        
        if not check_session_timestamp(request.POST['load_timestamp']):
            messages.warning(
                request, "[ERROR] Current session has timed out."
            )
            messages.warning(
                request, "[ERROR] No changes has been deployed."
            )
            return redirect("dcim:device_portmanager", pk=pk)
        
        device = self.get_object(pk=pk)
        if (device_data := get_device_data(device, request.user)) is None:
            self.handle_no_permission()
        
        switch: CiscoSwitch = get_switch(
            fqdn=device_data["fqdn"],
            community_string=device_data["community_string"],
            switch_type=device.device_type.manufacturer.name.lower(),
            portsec_max=device_data["portsec_max"],
            vlan_intervals=device_data["vlans"],
        )

        if not self.try_load_main_data(switch):
            messages.warning(f"Cannot load data from device.")
            messages.warning(
                request, "[FAILURE] Please try again or contact administrator."
            )
            return redirect("home")

        data = self.get_post_data(request)
        try:
            switch.deploy_interface_changes(data)
        except DataIntegrityError as e:
            LOGGER.error(f"DataIntegrityError: {e}")
            messages.warning(
                request,
                "[FAILURE] Please try again or contact administrator. (DataIntegrityError)",
            )
            return redirect("home")
        except Exception as e:
            LOGGER.error(f"Exception: {e}")
            messages.warning(
                request,
                "[FAILURE] Please try again or contact administrator. (DataIntegrityError)",
            )
            return redirect("home")

        if len(switch.success_set) == 0:
            messages.info(request, "Nothing has changed")
        elif len(switch.success_set) > 0 and "_deploy_save" in request.POST:
            switch.save()
        if len(switch.success_set) > 0:
            messages.success(
                request,
                f"[SUCCESS]: {len(switch.success_set)} change(s) were made. For more information visit the Change Log.",
            )
        if len(switch.failure_set) != 0:
            messages.warning(
                request,
                f"[FAILURE]: {len(switch.failure_set)} change(s) were not made. For more information visit the Change Log.",
            )

        self.create_change_log(
            user=request.user,
            device=device,
            changes=switch.success_set + switch.failure_set,
        )

        table = self.create_and_setup_table(switch, device)
        return render(
            request,
            self.get_template_name(),
            {
                "object": device,
                "table": table,
                "show_poe": switch.show_poe,
                "available_poe": switch.available_poe,
                "used_poe": switch.used_poe,
                "remaining_poe": switch.remaining_poe,
                "load_timestamp": get_session_timestamp()
            },
        )

    @staticmethod
    def create_change_log(
        user,
        device: Device,
        changes: List[Dict[str, str]],
    ):
        for change in changes:
            ChangeLog(
                user_object=user, 
                user=get_user_label(user), 
                device=device.name, 
                **change
            ).save()

    @staticmethod
    def create_and_setup_table(
        switch: GeneralSwitch, 
        device: Device
    ) -> InterfaceTable:
        interfaces = switch.get_interfaces()
        table = InterfaceTable(interfaces)
        table.vlans = switch.get_vlans()
        table.portsec_max = switch.portsec_max
        table.device = device
        if not switch.show_poe:
            table.exclude = (
                "if_poe_consum",
                "if_poe_alloc",
                "if_poe_max",
            )
        return table

    @staticmethod
    def get_post_data(request) -> POST_DATA:
        return {
            "if_index": request.POST.getlist("if_index"),
            "if_vlan": request.POST.getlist("if_vlan"),
            "if_description": request.POST.getlist("if_description"),
            "if_admin_status": request.POST.getlist("if_admin_status"),
            "if_ps_max": request.POST.getlist("if_ps_max"),
        }


#
# Interface MAC
#

class InterfaceVlanMacView(generic.ObjectView):
    queryset = Device.objects.all()
    template = "netbox_portmanager/mac.jinja"

    def get_required_permission(self):
        return get_permission_for_model(Device, 'portmanager')

    def get(self, request, device_pk, if_index, vlan_id):
        device = self.get_object(pk=device_pk)
        if (device_data := get_device_data(device, request.user)) is None:
            render(request, self.template, context={'macs': [], 'error': "Permission denied."})
        
        switch: CiscoSwitch = get_switch(
            fqdn=device_data["fqdn"],
            community_string=device_data["community_string"],
            switch_type=device.device_type.manufacturer.name.lower(),
            portsec_max=device_data["portsec_max"],
            vlan_intervals=device_data["vlans"],
        )
        macs, error = [], None
        try:
            macs = switch.get_interface_macs(vlan_id=vlan_id, if_index=if_index)
        except SNMPError as e:
            LOGGER.error(f'Cannot load data from device ({device})')
            error = f'Cannot load data from device ({e}).'

        return render(request, self.template, context={'macs': macs, 'error': error})


#
# Change Log
#

class ChangeLogListView(generic.ObjectListView):
    queryset = ChangeLog.objects.all()
    template_name = 'generic/object_list.html'
    table = ChangeLogTable
    actions = ('export',)
    filterset = ChangeLogFilterSet


@register_model_view(ChangeLog)
class ChangeLogView(generic.ObjectView):
    queryset = ChangeLog.objects.all()
    template_name = 'netbox_portmanager/changelog.jinja'


#
# User Change Log
#

class UserChangeLogListView(generic.ObjectListView):
    queryset = ChangeLog.objects.all()
    template_name = 'generic/object_list.html'
    table = ChangeLogTable
    actions = ('export',)
    filterset = ChangeLogFilterSet

    def get_required_permission(self):
        return get_permission_for_model(Device, 'portmanager')

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        if user.has_perms((perm,)):
            self.queryset = self.queryset.filter(user_object=user)
            return True
        return False
    


class UserChangeLogView(generic.ObjectView):
    queryset = ChangeLog.objects.all()
    template_name = 'netbox_portmanager/changelog.jinja'

    def get_required_permission(self):
        return get_permission_for_model(Device, 'portmanager')

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        if user.has_perms((perm,)):
            self.queryset = self.queryset.filter(user_object=user)
            return True
        return False

#
# Device Group
#

class DeviceGroupListView(generic.ObjectListView):
    queryset = DeviceGroup.objects.all()
    filterset = DeviceGroupFilterSet
    table = DeviceGroupTable
    template_name = 'generic/object_list.html'
    actions = ('add', )


class DeviceGroupView(generic.ObjectView):
    queryset = DeviceGroup.objects.all()
    template_name = 'netbox_portmanager/devicegroup.html'
    table = DeviceTable

    def get_extra_context(self, request, instance):
        user_groups = instance.user_groups.all()
        user_group_table = UserGroupTable(user_groups, orderable=False)

        users = instance.users.all()
        user_table = UserTable(users, orderable=False)

        all_users = (
            instance.users.all() 
            | User.objects.filter(groups__in=user_groups)
        ).distinct()
        all_users_table = UserTable(all_users, orderable=False)

        devices = instance.devices.all()
        device_table = DeviceTable(devices, orderable=False)
        
        return {
            'all_users_table': all_users_table,
            'user_table': user_table,
            'device_table': device_table,
            'user_group_table': user_group_table
        }


class DeviceGroupEditView(generic.ObjectEditView):
    queryset = DeviceGroup.objects.all()
    form = DeviceGroupForm
    template_name = 'generic/object_edit.html'


class DeviceGroupDeleteView(generic.ObjectDeleteView):
    queryset = DeviceGroup.objects.all()


#
# Secret
#

class SecretListView(generic.ObjectListView):
    queryset = Secret.objects.all()
    template_name = 'netbox_portmanager/secret_list.html'
    table = SecretTable
    filterset = SecretFilterSet
    actions = ('add', )
    
    def get_extra_context(self, request):
        return {
            'device_cf_name': DEVICE_CF_NAME
        }


class SecretView(generic.ObjectView):
    queryset = Secret.objects.all()
    template_name = 'netbox_portmanager/secret.html'

    def get_extra_context(self, request, instance):
        devices = instance.get_assigned_devices()
        table = DeviceTable(devices, orderable=False)
        return {
            'table': table
        }


class SecretEditView(generic.ObjectEditView):
    queryset = Secret.objects.all()
    form = SecretForm
    template_name = 'generic/object_edit.html'


class SecretDeleteView(generic.ObjectDeleteView):
    queryset = Secret.objects.all()


#
# User Group
#

class UserGroupListView(generic.ObjectListView):
    queryset = Group.objects.all()
    template_name = 'netbox_portmanager/usergroup_list.html'
    table = UserGroupTable
    actions = ()

    def get_required_permission(self):
        return get_permission_for_model(DeviceGroup, 'view')

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        if user.has_perms((perm,)):
            self.queryset = self.queryset.exclude(device_groups__isnull=True)
            return True
        return False


class UserGroupView(generic.ObjectView):
    queryset = Group.objects.all()
    template_name = 'netbox_portmanager/usergroup.html'

    def get_required_permission(self):
        return get_permission_for_model(DeviceGroup, 'view')

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        if user.has_perms((perm,)):
            self.queryset = self.queryset.exclude(device_groups__isnull=True)
            return True
        return False

    def get_extra_context(self, request, instance):
        users = User.objects.filter(groups=instance)
        user_table = UserTable(users, orderable=False)

        device_groups = DeviceGroup.objects.filter(user_groups=instance).distinct()
        device_group_table = DeviceGroupTable(device_groups, exclude=('actions', ), orderable=False)

        devices = Device.objects.filter(device_groups__user_groups=instance).distinct()
        device_table = DeviceTable(devices, orderable=False)

        return {
            'user_table': user_table,
            'device_group_table': device_group_table,
            'device_table': device_table,
        }


#
# User
#

class UserListView(generic.ObjectListView):
    queryset = User.objects.exclude(
        Q(device_groups__isnull=True) &
        Q(groups__device_groups__isnull=True)
    )
    template_name = 'generic/object_list.html'
    table = UserTable
    actions = ()

    def get_required_permission(self):
        return get_permission_for_model(DeviceGroup, 'view')

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        return user.has_perms((perm,))


class UserView(generic.ObjectView):
    queryset = User.objects.exclude(
        Q(device_groups__isnull=True) &
        Q(groups__device_groups__isnull=True)
    )
    template_name = 'netbox_portmanager/user.html'

    def get_required_permission(self):
        return get_permission_for_model(DeviceGroup, 'view')

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        return user.has_perms((perm,))

    def get_extra_context(self, request, instance):
        user_groups = instance.groups.exclude(device_groups__isnull=True)
        user_group_table = UserGroupTable(user_groups, exclude=('actions', ), orderable=False)
        
        device_groups = DeviceGroup.objects.filter(
            Q(user_groups__in=user_groups) |
            Q(users=instance)
        ).distinct()
        device_group_table = DeviceGroupTable(device_groups, exclude=('actions', ), orderable=False)

        devices = Device.objects.filter(
            Q(device_groups__in=device_groups) |
            Q(device_groups__user_groups__in=user_groups)
        ).distinct()
        device_table = DeviceTable(devices, orderable=False)
        return {
            'user_group_table': user_group_table,
            'device_group_table': device_group_table,
            'device_table': device_table,
            'userinfo': True
        }


class UserInfoView(generic.ObjectView):
    template_name = 'netbox_portmanager/user.html'
    queryset = User.objects.all()

    def get_required_permission(self):
        return get_permission_for_model(Device, 'portmanager')

    def has_permission(self):
        user = self.request.user
        perm = self.get_required_permission()
        return user.has_perms((perm,))

    def get(self, request):
        user = request.user
        return render(request, self.get_template_name(), {
            'object': user,
            'tab': self.tab,
            **self.get_extra_context(request, user),
        })

    def get_extra_context(self, request, instance):
        user_groups = instance.groups.exclude(device_groups__isnull=True)
        user_group_table = UserGroupTable(user_groups, exclude=('actions', ), orderable=False)
        
        device_groups = DeviceGroup.objects.filter(
            Q(user_groups__in=user_groups) |
            Q(users=instance)
        ).distinct()
        device_group_table = DeviceGroupTable(device_groups, exclude=('actions', ), orderable=False)

        devices = Device.objects.filter(
            Q(device_groups__in=device_groups) |
            Q(device_groups__user_groups__in=user_groups)
        ).distinct()
        device_table = DeviceTable(devices, orderable=False)
        return {
            'user_group_table': user_group_table,
            'device_group_table': device_group_table,
            'device_table': device_table,
        }
