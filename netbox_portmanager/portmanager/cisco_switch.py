from pysnmp.hlapi import *

from .general_switch import GeneralSwitch
from .interface import Interface
from .vlan import Vlan
from .utils import *
from .exceptions import SNMPError, DataIntegrityError

from typing import List, Dict, Optional, Union, Tuple
import re


RESULT = List[Dict[str, str]]
SNMP_RESULT = Dict[str, Dict[str, Union[str, int]]]
CONSUM_ALLOC = Optional[Tuple[int, int]]
SNMP_ARGS = Dict[str, Union[int, Tuple[str, type]]]

class CiscoSwitch(GeneralSwitch):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def get_interface_macs(self, vlan_id: int, if_index: int) -> List[str]:
        self.load_macs_data(vlan_id)
        results = []
        if_index = str(if_index)
        for index, mac in self.snmp_results[self.NAME_OID_DICT["dot1dTpFdbAddress"]].items():
            port_id = self.snmp_results[self.NAME_OID_DICT["dot1dTpFdbPort"]][index]
            index = self.snmp_results[self.NAME_OID_DICT["dot1dBasePortIfIndex"]].get(port_id)
            if index == if_index:
                splitted_mac = mac.upper()[2:]
                results.append(":".join([splitted_mac[i:i+2] for i in range(0, len(splitted_mac), 2)]))
        return results

    def load_macs_data(self, vlan_id: int) -> None:
        self.__get_macs_raw_data(credentials=f"{self.community_string}@{vlan_id}")

    def load_main_data(self) -> None:
        self.__get_main_raw_data()

        self.__load_vlans()
        self.__load_interfaces()

    def __load_vlans(self) -> None:
        for index, vlan_desc in self.snmp_results[self.NAME_OID_DICT["vtpVlanName"]].items():
            self.load_object(Vlan, index, self.vlans, {"vlan_index": int(index), "vlan_description": vlan_desc})
        self._filter_vlans()

    def __load_interfaces(self) -> None:
        self.available_poe = sum(self.snmp_results[self.NAME_OID_DICT["pethMainPsePower"]].values())
        if self.available_poe != 0:
            self.show_poe = True

        for if_index, if_name in self.snmp_results[self.NAME_OID_DICT["ifName"]].items():
            poe_consum, poe_alloc = None, None
            if (poe_info:=self.__get_consum_and_alloc_poe(if_name)) is not None:
                poe_consum, poe_alloc = poe_info
                self.used_poe += poe_alloc

            obj_kwargs = {
                "if_index": if_index,
                "if_name": if_name,
                "if_vlan": self.snmp_results[self.NAME_OID_DICT["ifType"]].get(if_index),
                "if_description": self.snmp_results[self.NAME_OID_DICT["ifAlias"]].get(if_index),
                "if_admin_status": self.snmp_results[self.NAME_OID_DICT["ifAdminStatus"]].get(if_index),
                "if_oper_status": self.snmp_results[self.NAME_OID_DICT["ifOperStatus"]].get(if_index),
                "if_vlan": self.snmp_results[self.NAME_OID_DICT["vmVlan"]].get(if_index),
                "if_trunk": self.snmp_results[self.NAME_OID_DICT["ifTrunk"]].get(if_index),
                "if_type": self.snmp_results[self.NAME_OID_DICT["ifType"]].get(if_index),
                "if_ps_enable": self.snmp_results[self.NAME_OID_DICT["cpsIfPortSecurityEnable"]].get(if_index),
                "if_ps_max": self.snmp_results[self.NAME_OID_DICT["cpsIfMaxSecureMacAddr"]].get(if_index),
                "if_poe_alloc": poe_alloc,
                "if_poe_consum": poe_consum,
            }
            self.load_object(Interface, if_index, self.interfaces, obj_kwargs)
        
        self.remaining_poe = max(self.available_poe - (self.used_poe / 1000), 0)
        self.used_poe /= 1000
        self._filter_interfaces()

    def __get_consum_and_alloc_poe(self, if_name: str) -> CONSUM_ALLOC:
        port_label = if_name.split("/")
        fst, second = None, None
        if len(port_label) >= 2 and (len(port_label) == 2 or port_label[1] == '0'):
            fst, second = port_label[0][-1], port_label[-1]
        if fst is not None and second is not None:
            snmp_node = f"{fst}.{second}"
            poe_alloc = self.snmp_results[self.NAME_OID_DICT["cpeExtPsePortPwrAllocated"]].get(snmp_node, None)
            poe_consum = self.snmp_results[self.NAME_OID_DICT["cpeExtPsePortPwrConsumption"]].get(snmp_node, None)
            if poe_alloc is not None and poe_consum is not None:
                return (poe_consum, poe_alloc)
        return None

    def deploy_interface_changes(self, data: Dict[str, List[str]]) -> None:
        if not self.check_interfaces_data(data):
            raise DataIntegrityError

        changes: List[Dict[str, str]] = []

        for index, if_index in enumerate(data["if_index"]):
            interface = self.interfaces.get(if_index)
            if interface is None or interface.if_enabled is False:
                self.failure_set.append({"component": "Internal Error", "details": f"Interface not found (SNMP index: {index}).", "status": "FAILURE"})
                continue

            change_vlan = data["if_vlan"][index]
            if change_vlan != "" and change_vlan.isdigit() and int(change_vlan) != interface.if_vlan:
                if change_vlan not in self.vlans or not self.vlans[change_vlan].vlan_enabled:
                    self.failure_set.append({"interface": interface.if_name, "component": "vlan", "before": interface.if_vlan, "after": change_vlan, "status": "FAILURE"})
                else:
                    changes.append((if_index, "if_vlan", "vlan", interface.if_vlan, self.NAME_OID_DICT["vmVlan"], int(change_vlan)))

            change_description = data["if_description"][index]
            if change_description != interface.if_description:
                if not bool(re.search(r'^[\x00-\x7F]*$', change_description)):
                    self.failure_set.append({"interface": interface.if_name, "component": "description", "before": interface.if_description, "after": change_description, "status": "FAILURE"})
                else:
                    changes.append((if_index, "if_description", "description", interface.if_description, self.NAME_OID_DICT["ifAlias"], change_description))

            change_admin_status = data["if_admin_status"][index]
            if change_admin_status != "" and change_admin_status.isdigit() and int(change_admin_status) != interface.if_admin_status:
                if change_admin_status not in ["1", "2"]:
                    self.failure_set.append({"interface": interface.if_name, "component": "admin status", "before": interface.if_admin_status, "after": change_admin_status, "status": "FAILURE"})
                else:
                    changes.append((if_index, "if_admin_status", "admin status", interface.if_admin_status, self.NAME_OID_DICT["ifAdminStatus"], int(change_admin_status)))

            change_if_ps_max = str(data["if_ps_max"][index])
            if change_if_ps_max != "" and change_if_ps_max.isdigit() and int(change_if_ps_max) != interface.if_ps_max:
                if interface.if_ps_enable == 2 or not change_if_ps_max.isdigit() or int(change_if_ps_max) > self.portsec_max:
                    self.failure_set.append({"interface": interface.if_name, "component": "portsec", "before": interface.if_ps_max, "after": change_if_ps_max, "status": "FAILURE"})
                else:
                    changes.append((if_index, "if_ps_max", "portsec max", interface.if_ps_max, self.NAME_OID_DICT["cpsIfMaxSecureMacAddr"], int(change_if_ps_max)))
                    
        self.__set_changes(changes)

    def __set_changes(self, changes: List[Dict[str, str]]) -> None:
        
        for if_index, if_attr, if_verbose, if_value_before, oid_root, value in changes:
            oid = f"{oid_root}.{if_index}"
            change = self.__get_default_change()
            change["interface"] = self.interfaces[if_index].if_name
            change["component"] = if_verbose
            change["before"] = if_value_before
            change["after"] = value
            try:
                snmp_set(
                    fqdn=self.fqdn,
                    oid=oid,
                    value=value,
                    credentials=self.community_string,
                )
                self.interfaces[if_index].__setattr__(if_attr, value)
                self.success_set.append(change)
                change["status"] = "SUCCESS"
            except SNMPError as e:
                change["details"] = str(e)
                change["status"] = "FAILURE"
                self.failure_set.append(change)

    def save(self) -> None:
        change = self.__get_default_change()
        change["component"] = "Save Running Config"
        try:
            snmp_set(
                fqdn=self.fqdn,
                credentials=self.community_string,
                value=1,
                oid=self.NAME_OID_DICT["writeMem"]
            )
            self.success_set.append(change)
            change["status"] = "SUCCESS"
        except SNMPError as e:
            change["details"] = str(e)
            change["status"] = "FAILURE"
            self.failure_set.append(change)

    @GeneralSwitch.call_snmp_walk
    def __get_main_raw_data(self) -> SNMP_ARGS:
        return [
            {
                "oids": {
                    self.NAME_OID_DICT["ifName"]       : str, 
                    self.NAME_OID_DICT["ifType"]       : int, 
                    self.NAME_OID_DICT["ifAlias"]      : str,
                    self.NAME_OID_DICT["ifAdminStatus"]: int, 
                    self.NAME_OID_DICT["ifOperStatus"] : int, 
                    self.NAME_OID_DICT["vmVlan"]       : int, 
                    self.NAME_OID_DICT["cpsIfMaxSecureMacAddr"]  : int, 
                    self.NAME_OID_DICT["cpsIfPortSecurityEnable"]: int, 
                    self.NAME_OID_DICT["ifTrunk"]: int,
                },
            },
            {
                "oids": {
                    self.NAME_OID_DICT["cpeExtPsePortPwrAllocated"]: int,
                    self.NAME_OID_DICT["cpeExtPsePortPwrConsumption"]: int
                },
                "node_count": 2
            },
            {
                "oids": {
                    self.NAME_OID_DICT["vtpVlanName"]: str
                }
            },
            {
                "oids": {
                    self.NAME_OID_DICT["pethMainPsePower"]: int,
                }
            }
        ]

    @GeneralSwitch.call_snmp_walk
    def __get_macs_raw_data(self) -> SNMP_ARGS:
        return [
            {
                "oids": {
                    self.NAME_OID_DICT["dot1dTpFdbPort"]: str,
                    self.NAME_OID_DICT["dot1dTpFdbAddress"]: str
                },
                "node_count": 6
            },
            {
                "oids": {
                    self.NAME_OID_DICT["dot1dBasePortIfIndex"]: str,
                }
            }
            
        ]

    
    
    def __get_default_change(self) -> Dict[str, Optional[str]]:
        return {
            "interface": None,
            "component": None,
            "before": None,
            "after": None,
            "details": None,
            "status": "UNKNOWN"
        }
