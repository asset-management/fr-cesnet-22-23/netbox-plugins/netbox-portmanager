from django.conf import settings
import logging


#
# Logger
#

LOGGER = logging.getLogger(__name__)


#
# Plugin Config
#

PLUGIN_CONFIG = settings.PLUGINS_CONFIG["netbox_portmanager"]
GLOBAL_COMMUNITY_STRING = PLUGIN_CONFIG["global_community_string"]
CHANGELOG_DATETIME_FORMAT = PLUGIN_CONFIG["changelog_datetime_format"]
DEVICE_FQDN_ATTR = PLUGIN_CONFIG["device_fqdn_attr"]
DEVICE_CF_NAME = PLUGIN_CONFIG["device_cf_name"]
DEFAULT_DG_PREFIX = PLUGIN_CONFIG["default_dg_prefix"]
DEFAULT_DG_SUFIX = PLUGIN_CONFIG["default_dg_sufix"]
SESSION_TIMEOUT = PLUGIN_CONFIG["session_timeout"]
SNMP_TIMEOUT = PLUGIN_CONFIG["snmp_timeout"]

CONFIG_TEMPLATE = {
    "global_community_string": lambda value: value is None or (isinstance(value, str) and len(value) > 0),
    "changelog_datetime_format": lambda value: isinstance(value, str) and len(value) > 0,
    "device_fqdn_attr": lambda value: isinstance(value, str) and len(value) > 0,
    "device_custom_field_name": lambda value: isinstance(value, str) and len(value) > 0,
}
