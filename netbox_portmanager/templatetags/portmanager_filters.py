from django import template
from django.contrib.auth.models import User, Group

from dcim.models import Device
from typing import List
from netbox_portmanager.models import DeviceGroup, Secret
from netbox_portmanager.utils import get_user_label
from netbox_portmanager.constants import DEVICE_CF_NAME

register = template.Library()




@register.filter(name="keyvalue")
def key_value(dictionary, key):    
    return dictionary.get(str(key), None)


@register.filter(name="getpsmax")
def get_portsec_max(if_ps_max, if_ps_enable):
    return "" if if_ps_enable == 2 else if_ps_max


@register.filter(name="getdevicegroups")
def get_device_groups(pk) -> List[DeviceGroup]:
    return [dg for dg in Device.objects.get(pk=pk).device_groups.all()]


@register.filter(name="getbadge")
def get_badge(status: str) -> str:
    if status == "SUCCESS":
        return "bg-green"
    elif status == "FAILURE":
        return "bg-red"
    return "bg-blue"

@register.filter(name="userlabel")
def user_label(user: User) -> str:
    return get_user_label(user)

@register.filter(name="users")
def get_users(user_group: Group) -> str:
    return User.objects.filter(groups=user_group)

@register.filter(name="get_devices_of_community_string")
def get_devices_of_community_string(community_string: Secret) -> List[Device]:
    kwargs = {
        f"custom_field_data__{DEVICE_CF_NAME}": community_string.id
    }
    return Device.objects.filter(**kwargs)