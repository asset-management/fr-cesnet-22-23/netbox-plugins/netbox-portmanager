IF_TABLE_SNMP_INDEX="""
<input id="id_snmp_index" value="{{ record.if_index }}" name="if_index" type="hidden" autocomplete="off" {% if not record.if_enabled %}disabled{% endif %}>
"""

CLT_STATUS = """
{% load portmanager_filters %}
<span class="badge {{ record.status|getbadge }}">{{ record.status }}</span>
"""


DGT_DEVICE_GROUP = """
<span class="badge bg-blue" style="margin: 3px;">
    <a href="{% url 'plugins:netbox_portmanager:devicegroup' pk=record.pk %}">
        {{ record.name }}
    </a>
</span>
"""


DGT_USER_GROUPS = """
{% for user_group in record.user_groups.all %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{{ user_group.get_absolute_url }}">
            {{ user_group.name }}
        </a>
    </span>
{% endfor %}
"""


DGT_DEVICES = """
{% for device in record.devices.all %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{% url 'dcim:device_portmanager' pk=device.pk %}">
            {{ device.name }}
        </a>
    </span>
{% endfor %}
"""

DGT_USERS = """
{% load portmanager_filters %}
{% for user in record.users.all %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{% url 'plugins:netbox_portmanager:user' pk=user.pk %}">
            {{ user|userlabel }}
        </a>
    </span>
{% endfor %}
"""


UGT_DEVICE_GROUPS = """
{% for device_group in record.device_groups.all %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{{ device_group.get_absolute_url }}">
            {{ device_group.name }}
        </a>
    </span>
{% endfor %}
"""


UGT_USERS = """
{% load portmanager_filters %}
{% for user in record.users.all %}
    <span class="badge bg-blue" style="margin: 3px;">{{ user|userlabel }}</span>
{% endfor %}
"""

UGT_NAME = """
<a href="{{ record.get_absolute_url }}">
    {{ record.name }}
</a>
"""


DT_DEVICE = """
<span class="badge bg-blue" style="margin: 3px;">
    <a href="{% url 'dcim:device_portmanager' pk=record.pk %}">
        {{ record.name|default:'<span class="badge bg-info">Unnamed device</span>' }}
    </a>
</span>
"""


IF_TABLE_DESC = """
<input id="id_description" class="form-control" maxlength="200" value="{{ record.if_description }}" name="if_description" type="text" {% if not record.if_enabled %}pattern="[ -ÿ]+"{% endif %} placeholder="Description" title="Description in ASCII format." autocomplete="off" {% if not record.if_enabled %}disabled{% endif %}>
"""


IF_TABLE_VLAN = """
{% load portmanager_filters %}
{% with act_vlan=table.vlans|keyvalue:record.if_vlan %}
{% if act_vlan is none %}
  <select id="id_vlan" name="if_vlan" class="netbox-static-select" autocomplete="off" required disabled>
      <option value=" " selected="selected" disabled>N/A</option>
  </select>
{% elif not record.if_enabled %}
    <select id="id_vlan" name="if_vlan" class="netbox-static-select" autocomplete="off" required disabled>
        <option value="{{ act_vlan.vlan_index }}" selected="selected">{{ act_vlan.vlan_index }} {{ act_vlan.vlan_description }}</option>
    </select>
{% else %}
    <select id="id_vlan" name="if_vlan" class="netbox-static-select" autocomplete="off" required>
        <option value="{{ act_vlan.vlan_index }}" selected="selected">{{ act_vlan.vlan_index }} {{ act_vlan.vlan_description }}</option>
        {% for vlan_index, vlan in table.vlans.items %}
            {% if vlan.vlan_enabled and vlan.vlan_index != record.if_vlan %}
                <option value="{{ vlan.vlan_index }}">{{ vlan.vlan_index }} {{ vlan.vlan_description }}</option>
            {% endif %}
        {% endfor %}
    </select>
{% endif %}
{% endwith %}
<span class="ss-arrow">
  <span class="ss-arrow"></span>
</span>
"""


IF_TABLE_PS_MAX = """
{% load portmanager_filters %}
<input id="id_ps_max" class="form-control" value="{{ record.if_ps_max|getpsmax:record.if_ps_enable }}" name="if_ps_max" type="number" {% if record.if_enabled %}min="1" max="{{ table.portsec_max }}"{% endif %} placeholder="N/A" autocomplete="off" {% if not record.if_enabled %}disabled{% endif %}/>
"""


IF_TABLE_ADMIN_STATUS = """
<select id="id_admin_status" name="if_admin_status" class="form-control" autocomplete="off" required {% if not record.if_enabled %}disabled{% endif %}>
    <option value="1" {% if record.if_admin_status == 1 %}selected="selected" {% endif %}>ON</option>
    <option value="2" {% if record.if_admin_status == 2 %}selected="selected" {% endif %}>OFF</option>
</select>
"""


IF_TABLE_STATE = """
{% load static %} 
{% if record.if_admin_status == 2 %}
    <img src="{% static 'netbox_portmanager/img/disabled.png'%}" alt="disabled" title="DISABLED" class="img-fluid" style="width:40px;">
{% elif record.if_oper_status == 1 %}
    <img src="{% static 'netbox_portmanager/img/connected.png'%}" alt="connected" title="CONNECTED" class="img-fluid" style="width:40px;">
{% else %}
    <img src="{% static 'netbox_portmanager/img/notconnected.png'%}" alt="notconnected" title="NOTCONNECTED" class="img-fluid" style="width:40px;">
{% endif %}
"""


IF_TABLE_SHOW_MACS="""
{% load helpers %}
{% if not record.if_enabled %}
    <div class="btn btn-sm btn-outline-dark" disabled title="No access.">Show MACs</div>
{% else %}
<a href="#" hx-history=false hx-get="{% url 'plugins:netbox_portmanager:interface_mac' device_pk=table.device.pk if_index=record.if_index vlan_id=record.if_vlan %}" data-bs-target="#htmx-modal" data-bs-toggle="modal" hx-target="#htmx-modal-content" class="btn btn-sm btn-success">
  &nbsp;Show MACs
</a>
{% endif %}
"""


DT_DEVICE_GROUPS = """
{% for device in record.device_groups.all %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{{ device.get_absolute_url }}">
            {{ device.name }}
        </a>
    </span>
{% endfor %}
"""


CST_NAME = """
<a href="{{ record.get_absolute_url }}">
    {{ record.name }}
</a>
"""

CST_DEVICES = """
{% load portmanager_filters %}
{% for device in record|get_devices_of_community_string %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{{ device.get_absolute_url }}">
            {{ device.name }}
        </a>
    </span>
{% endfor %}
"""


SDT_DEVICE_GROUPS = """
{% for device in record.device_groups.all %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{{ device.get_absolute_url }}">
            {{ device.name }}
        </a>
    </span>
{% endfor %}
"""

SDT_NAME = """
<a href="{{ device.get_absolute_url }}">
    {{ record.name|default:'<span class="badge bg-info">Unnamed device</span>' }}
</a>
"""


UT_USERNAME = """
{% load portmanager_filters %}
<span class="badge bg-blue" style="margin: 3px;">
    <a href="{% url 'plugins:netbox_portmanager:user' pk=record.pk %}">
        {{ record|userlabel }}
    </a>
</span>
"""

UGT_NAME = """
<span class="badge bg-blue" style="margin: 3px;">
    <a href="{% url 'plugins:netbox_portmanager:usergroup' pk=record.pk %}">
        {{ record }}
    </a>
</span>
"""

UGT_USERS = """
{% load portmanager_filters %}
{% for user in record|users  %}
    <span class="badge bg-blue" style="margin: 3px;">
        <a href="{% url 'plugins:netbox_portmanager:user' pk=user.pk %}">
            {{ user|userlabel }}
        </a>
    </span>
{% endfor %}
"""