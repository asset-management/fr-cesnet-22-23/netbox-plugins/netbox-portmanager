from setuptools import find_packages, setup

setup(
    name='netbox-portmanager',
    version='0.3.5',
    description='Netbox Plugin - Portmanager',
    long_description='Netbox Plugin - Portmanager',
    url='https://gitlab.ics.muni.cz/asset-management/fr-cesnet-2022-2023/netbox-portmanager',
    download_url='https://www.pypi.org/project/netbox-portmanager/',
    author='Michal Drobny',
    author_email='drobny@ics.muni.cz',
    license='Apache 2.0',
    install_requires=[
        'pysnmp==4.4.12',
        'pyasn1==0.4.8',
    ],
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Framework :: Django",
    ],
    python_requires=">=3.9",
    zip_safe=False,
)
