from django import forms
from django.utils.translation import gettext as _
from django.contrib.auth.models import User, Group

from utilities.forms.fields import DynamicModelMultipleChoiceField, DynamicModelChoiceField
from dcim.models import Device, DeviceRole
from netbox.forms import NetBoxModelForm

from ..portmanager.vlan import VLANInterval
from ..models import *
from ..forms import UserMultipleChoiceField, UserGroupMultipleChoiceField
from ..constants import DEFAULT_DG_PREFIX, DEFAULT_DG_SUFIX

from .forms import UserMultipleChoiceField, UserGroupMultipleChoiceField


class SecretForm(NetBoxModelForm):
    name = forms.CharField(
        label='Name',
        max_length=100,
        required=True,
    )
    _secret = forms.CharField(
        label='Secret',
        required=False,
    )
    class Meta:
        model = Secret
        fields = [
            'name', '_secret', 'tags', 
        ]


class DeviceGroupForm(NetBoxModelForm):
    name = forms.CharField(
        label="Name",
        max_length=80,
        required=True,
        help_text="Name of the Device Group."
    )
    user_groups = UserGroupMultipleChoiceField (
        label="User Groups",
        queryset=Group.objects.filter(
            name__startswith=DEFAULT_DG_PREFIX,
            name__endswith=DEFAULT_DG_SUFIX,
        ),
        required=False,
        help_text="User Groups assigned to the Device Group."
    )
    users = UserMultipleChoiceField(
        label='Users',
        queryset=User.objects.all(),
        required=False,
        help_text='Users assigned to the User Group.',
    )
    vlans = forms.CharField(
        label="VLANs",
        max_length=300,
        initial=1,
        required=False,
        help_text="Interval of VLANs that can be set. Example: [1-25,29,35-88]",
    )
    portsec_max = forms.IntegerField(
        label="Portsec MAX",
        min_value=1,
        max_value=16384,
        required=True,
        help_text="Maximal adjustable port security value on any port in this Device Group."
    )
    device_role = DynamicModelChoiceField(
        queryset=DeviceRole.objects.all(),
        required=False,
    )
    devices = DynamicModelMultipleChoiceField(
        label='Devices',
        queryset=Device.objects.all(),
        required=False,
        help_text="Devices assigned to the Device Group.",
        query_params={
            'role_id': '$device_role'
        }
    )

    fieldsets = (
        ('Device Group', ('name', 'vlans', 'portsec_max', 'tags', ), ),
        ('Devices', ('device_role', 'devices', ), ),
        ('Users', ('user_groups', 'users', ), )
    )

    class Meta:
        model = DeviceGroup
        fields = [
            'name', 'vlans', 'portsec_max', 'device_role', 'devices', 'user_groups', 'users',
        ]
    
    def clean(self):
        vlans = self.cleaned_data.get("vlans")
        if not VLANInterval.check_syntax(vlans):
            self._errors["vlans"] = self.error_class([
                    'Bad format'])

        return super().clean()


