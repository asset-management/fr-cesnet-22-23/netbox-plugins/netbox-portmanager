from django import forms

from ..utils import get_user_label

class UserMultipleChoiceField(forms.ModelMultipleChoiceField):
    """
    Selection field for one or more content types.
    """
    widget = forms.SelectMultiple

    def __init__(self, queryset, *args, **kwargs):
        # Order UserGroups by their last_name
        queryset = queryset.order_by('last_name')
        super().__init__(queryset, *args, **kwargs)

    def label_from_instance(self, obj):
        return get_user_label(obj)

class UserGroupMultipleChoiceField(forms.ModelMultipleChoiceField):
    """
    Selection field for one or more content types.
    """
    widget = forms.SelectMultiple

    def __init__(self, queryset, *args, **kwargs):
        # Order UserGroups by their name
        queryset = queryset.order_by('name')
        super().__init__(queryset, *args, **kwargs)

    def label_from_instance(self, obj):
        return obj.name
