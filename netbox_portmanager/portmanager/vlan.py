from typing import Tuple, List
import re
from .exceptions import VLANIntervalSyntaxError

class Vlan:
    def __init__(
        self,
        vlan_index: int,
        vlan_description: str,
        vlan_enabled: bool=False,
    ) -> None:
        self.vlan_index = vlan_index
        self.vlan_description = vlan_description
        self.vlan_enabled = vlan_enabled
    
    def get_table_info(self) -> Tuple[str, str]:
        return {
            "vlan_index": self.vlan_index,
            "vlan_description": self.vlan_description,
            "vlan_enabled": self.vlan_enabled
        }


class VLANInterval:
    SYNTAX_PATTERN = "^\[(|\d+|\d+-\d+)(,\d+|,\d+-\d+)*\]$"
    def __init__(self, interval_strings: List[str]) -> None:
        self.intervals: List[Tuple[int, int]] = []
        for interval_string in interval_strings:
            if not self.check_and_build(interval_string=interval_string):
                raise VLANIntervalSyntaxError(interval_string)

    def get_intervals(self) -> List[Tuple[int, int]]:
        return self.intervals

    @staticmethod
    def check_syntax(interval_string: str) -> bool:
        return re.search(VLANInterval.SYNTAX_PATTERN, VLANInterval.replace_spaces(interval_string)) is not None

    @staticmethod
    def replace_spaces(interval_string: str) -> str:
        return interval_string.replace(" ", "")

    def check_and_build(self, interval_string: str) -> bool:
        interval_string = self.replace_spaces(interval_string)
        if not self.check_syntax(interval_string):
            return False

        interval_string = interval_string[1:-1]
        if len(interval_string) == 0:
            return True
        for vlan_id in interval_string.split(","):
            if "-" in vlan_id:
                min_id, max_id = vlan_id.split("-")
            else:
                min_id = max_id = int(vlan_id)
            self.intervals.append((int(min_id), int(max_id)))
        return True

    def check_boundaries(self, vlan: 'Vlan') -> bool:
        return any(
            map(
                lambda interval: interval[0] <= int(vlan.vlan_index) <= interval[1], 
                self.intervals
            )
        )
