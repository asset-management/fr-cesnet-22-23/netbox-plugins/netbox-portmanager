from netbox.api.routers import NetBoxRouter
from .views import DeviceGroupViewSet, ChangeLogViewSet, SecretViewSet

router = NetBoxRouter()

router.register('device-group', DeviceGroupViewSet)
router.register('changelog', ChangeLogViewSet)
router.register('secret', SecretViewSet)

urlpatterns = router.urls